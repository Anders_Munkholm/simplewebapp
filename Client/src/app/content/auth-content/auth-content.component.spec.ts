import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthContentComponent } from './auth-content.component';
import { AuthContentService } from './auth-content.service';
import { AsyncGoogleAPIService } from '../login/google/async-google-api.service';
import { AuthService } from '../../routing/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AuthContentComponent', () => {
  let component: AuthContentComponent;
  let fixture: ComponentFixture<AuthContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      declarations: [ AuthContentComponent ],
      schemas: [ NO_ERRORS_SCHEMA],
      providers:[
        AsyncGoogleAPIService, 
        AuthService,
        { provide: Router, useClass: RouterStub },
        {provide:ActivatedRoute,useValue: {params: of({id: 123})}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


class RouterStub {
  navigateByUrl(url: string) { return url; }
}
