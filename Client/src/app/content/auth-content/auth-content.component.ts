import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../routing/auth/auth.service';
import { AuthGuard } from '../../routing/auth/auth-guard.service';
import { AsyncGoogleAPIService } from '../login/google/async-google-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthContentService } from './auth-content.service';

@Component({
  selector: 'app-auth-content',
  templateUrl: './auth-content.component.html',
  styleUrls: ['./auth-content.component.css'],
  providers:[AuthContentService]
})
export class AuthContentComponent implements OnInit {
  constructor(private authService:AuthService, 
    private asyncGoogleApiService:AsyncGoogleAPIService, 
    private route: ActivatedRoute,
    private router:Router,
    private authContentService:AuthContentService) {
      this.changeHeaderStatus();

     }

  ngOnInit() {
    //can't do init here unless we do a custom init
  }

  public changeHeaderStatus() {
    this.authContentService.changeHeaderStatus();
  }

  public logout() {
    this.authContentService.logout();
    //reloading the whole site on logout, because google api gets out of angular6's scope on the logout page
    
  }

}
