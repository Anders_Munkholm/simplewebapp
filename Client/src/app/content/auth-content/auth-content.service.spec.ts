import { TestBed, inject } from '@angular/core/testing';

import { AuthContentService } from './auth-content.service';
import { AsyncGoogleAPIService } from '../login/google/async-google-api.service';

describe('AuthContentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthContentService,AsyncGoogleAPIService]
    });
  });

  it('should be created', inject([AuthContentService], (service: AuthContentService) => {
    expect(service).toBeTruthy();
  }));
});
