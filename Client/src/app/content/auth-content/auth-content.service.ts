import { Injectable } from '@angular/core';
import { AsyncGoogleAPIService } from '../login/google/async-google-api.service';
import { Subject ,  Observable } from 'rxjs';
import { AuthService } from '../../routing/auth/auth.service';
import { environment } from '../../../environments/environment.prod';
import { base_url } from '../../../base-url/base-url';

@Injectable()
export class AuthContentService {

  constructor(private authService:AuthService,private asyncGoogleApiService: AsyncGoogleAPIService) { }

  public changeHeaderStatus() {
    //do checks to see if it's something else than google
    this.asyncGoogleApiService.loginStatus(true);
  }

  public logout() {
    this.asyncGoogleApiService.logoutGoogle();
    this.authService.logout();
    this.reloadWebsite();
  }

  public reloadWebsite() {
      window.location.href = base_url.url
  }

}
