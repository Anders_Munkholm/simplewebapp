import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoneCalculatorComponent } from './stone-calculator.component';
import { MaterialModules } from '../../../app.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('StoneCalculatorComponent', () => {
  let component: StoneCalculatorComponent;
  let fixture: ComponentFixture<StoneCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[MaterialModules,BrowserAnimationsModule],
      declarations: [ StoneCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoneCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
