import { TestBed, inject } from '@angular/core/testing';

import { StoneCalculatorService } from './stone-calculator.service';

describe('StoneCalculatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StoneCalculatorService]
    });
  });

  it('should be created', inject([StoneCalculatorService], (service: StoneCalculatorService) => {
    expect(service).toBeTruthy();
  }));
});
