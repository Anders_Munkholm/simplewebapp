import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  email:string = "Andersmunkholm@outlook.dk";
  phone:string = "22646228";
  linkedin:string = "https://dk.linkedin.com/in/anders-munkholm-35b31ba4"
  bitbucket:string = "https://bitbucket.org/Anders_Munkholm/";
  constructor() { }

  ngOnInit() {
  }


  public navigateLinkedin(): void {
    window.location.href = this.linkedin;
  }

  public navigateBitbucket(): void {
    window.location.href = this.bitbucket;
  }
}
