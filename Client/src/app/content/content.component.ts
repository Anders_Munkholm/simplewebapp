import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContentService } from './content.service';
import { Subject, Observable } from 'rxjs';
import { CookieService } from '../cookies/cookie.service';
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css'],
  providers:[ContentService,CookieService]
})
export class ContentComponent implements OnInit {
  
  LawNotificationEnabled:boolean = false;
  constructor(private contentService:ContentService,private cookieService:CookieService) { }

  ngOnInit() {
    this.CookieNotification();

  }

  public CookieNotification() : void {
    this.checkCookie();
    this.initLawNotificationListener();
  }

  public initLawNotificationListener():void {
    this.contentService.getLawNotificationEnabledStream().subscribe(
      enabled => {
        this.LawNotificationEnabled = enabled;
      }
    )
  }

  public checkCookie() {
    this.LawNotificationEnabled = !this.cookieService.cookieExist();
  }

  
  

}
