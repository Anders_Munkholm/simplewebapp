import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class ContentService {

  //observable sources
  private lawNotificationEnabled:Subject<boolean> = new Subject<boolean>();
  //observable streams
  private lawNotificationEnabled$:Observable<boolean> = this.lawNotificationEnabled.asObservable();

  constructor() { }


  public nextLawNotificationEnabled(value:boolean):void {
    this.lawNotificationEnabled.next(value);
  }

  public getLawNotificationEnabledStream():Observable<boolean> {
    return this.lawNotificationEnabled$;
  }

 


}
