import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookieInformationComponent } from './cookie-information.component';
import { MaterialModules } from '../../../app.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CookieInformationService } from './cookie-information.service';

describe('CookieInformationComponent', () => {
  let component: CookieInformationComponent;
  let fixture: ComponentFixture<CookieInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[MaterialModules,BrowserAnimationsModule],
      declarations: [ CookieInformationComponent ],
      providers:[CookieInformationService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookieInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
