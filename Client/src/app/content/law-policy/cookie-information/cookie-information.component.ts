import { Component, OnInit } from '@angular/core';
import { Cookie } from '../../../model/Cookie';
import { CookieInformationService } from './cookie-information.service';

@Component({
  selector: 'app-cookie-information',
  templateUrl: './cookie-information.component.html',
  styleUrls: ['./cookie-information.component.css'],
  providers:[CookieInformationService]
})
export class CookieInformationComponent implements OnInit {
  cookies:Cookie[];
  constructor(private cookieInformationService: CookieInformationService) { }

  ngOnInit() {
    this.cookies = this.cookieInformationService.getCookies();
  }

  displayedColumns = ['name', 'description'];
  

  

}
