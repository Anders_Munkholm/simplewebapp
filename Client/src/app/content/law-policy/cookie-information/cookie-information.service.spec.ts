import { TestBed, inject } from '@angular/core/testing';

import { CookieInformationService } from './cookie-information.service';

describe('CookieInformationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CookieInformationService]
    });
  });

  it('should be created', inject([CookieInformationService], (service: CookieInformationService) => {
    expect(service).toBeTruthy();
  }));
});
