import { Injectable } from '@angular/core';
import { Cookie } from '../../../model/Cookie';

@Injectable()
export class CookieInformationService {

  constructor() { }

  getCookies() : Cookie[] {
    let cookies = new Array<Cookie>();
    cookies.push(new Cookie("ARRAffinity", "This cookie is used to connect you to the correct server", "Microsoft Azure,AndersMunkholm.com"))
    cookies.push(new Cookie("policy_accepted", "This cookie is stored once you accept cookies. It is used in order not to spam you with cookie notifications", "AndersMunkholm.com"))
    cookies.push(new Cookie("G_AUTHUSER_H", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("G_ENABLED_IDPS", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("1P_JAR", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("APISID", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("HSID", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("NID", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("OTZ", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("SAPISID", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("SID", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("SIDCC", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("SSID", "Used for the login", "accounts.google.com, Andersmunkholm.com"));
    cookies.push(new Cookie("ACCOUNT_CHOOSER", "Used for the login", "accounts.google.com"));
    return cookies;
  }
}
