import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GDPRComponent } from './gdpr.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MaterialModules } from '../../../app.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('GDPRComponent', () => {
  let component: GDPRComponent;
  let fixture: ComponentFixture<GDPRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[MaterialModules,BrowserAnimationsModule],
      declarations: [ GDPRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GDPRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
