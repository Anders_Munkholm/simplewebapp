import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gdpr',
  templateUrl: './gdpr.component.html',
  styleUrls: ['./gdpr.component.css']
})
export class GDPRComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  navigateServiceWorker() {
    window.location.href = "https://developers.google.com/web/fundamentals/primers/service-workers/";
  }
}
