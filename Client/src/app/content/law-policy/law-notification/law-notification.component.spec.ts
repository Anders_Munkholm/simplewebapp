import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawNotificationComponent } from './law-notification.component';
import { Router } from '@angular/router';
import { MaterialModules } from '../../../app.module';
import { ContentService } from '../../content.service';

describe('CookieNotificationComponent', () => {
  let component: LawNotificationComponent;
  let fixture: ComponentFixture<LawNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[MaterialModules],
      declarations: [ LawNotificationComponent ],
      providers:[
        ContentService,
        { provide: Router, useClass: RouterStub },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

class RouterStub {
  navigate(url: any) { return url; }
}