import { Component, OnInit } from '@angular/core';
import { LawNotificationService } from './law-notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-law-notification',
  templateUrl: './law-notification.component.html',
  styleUrls: ['./law-notification.component.css'],
  providers:[LawNotificationService]
})
export class LawNotificationComponent implements OnInit {
  message:string;
  constructor(private lawNotificationService:LawNotificationService,private router:Router) { }

  ngOnInit() {
    this.message = this.lawNotificationService.getLawMessage();
  }

  accept() {
    this.lawNotificationService.closeComponent();
  }

  readMore() {
    this.router.navigate(['law-policies'])
  }

}
