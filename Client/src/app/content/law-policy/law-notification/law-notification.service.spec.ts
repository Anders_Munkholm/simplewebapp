import { TestBed, inject } from '@angular/core/testing';

import { LawNotificationService } from './law-notification.service';
import { ContentService } from '../../content.service';

describe('LawNotificationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LawNotificationService,ContentService]
    });
  });

  it('should be created', inject([LawNotificationService], (service: LawNotificationService) => {
    expect(service).toBeTruthy();
  }));
});
