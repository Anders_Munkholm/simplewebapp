import { Injectable } from '@angular/core';
import { ContentService } from '../../content.service';
import { CookieService } from '../../../cookies/cookie.service';

@Injectable()
export class LawNotificationService {

  
  constructor(private contentService: ContentService,private cookieService:CookieService) { }


  getLawMessage() :string {
    return "We use cookies to deliver our service." +
     " We use cookies that enable you to login with Google." +
     " We use cookies to improve the user experience." +
     " We create statistics based on the users to improve our service." +
     " We use a service worker to deliver our service, it allows you to access the website without a connection after a single visit.";
  }

  closeComponent():void {
    this.rememberClosed();
    this.contentService.nextLawNotificationEnabled(false);
  }

  rememberClosed() {
    this.cookieService.setAcceptedPoliciesCookie();
  }
}
