import { Subject ,  Observable, of } from "rxjs";
import { Injectable } from '@angular/core';
import { environment } from "../../../../environments/environment";
const url = "https://apis.google.com/js/platform.js?onload=init"
const prodClientId = "287016006027-bes4dnib6ibmb1ljtdid77l7kfi819rt.apps.googleusercontent.com";
const devClientId = "593503422572-s4c1t81ouq1j66i179599peddq3rnjbc.apps.googleusercontent.com";
declare const gapi: any;

@Injectable({
    providedIn: 'root',
  })
export class AsyncGoogleAPIService {

    private asyncConSubject: Subject<any> = new Subject<any>();
    private asyncConStream: Observable<any> = this.asyncConSubject.asObservable();
    private googleStatusSubject: Subject<boolean> = new Subject<boolean>();
    private googleStatusStream: Observable<boolean> = this.googleStatusSubject.asObservable();
    private user: any;
    public gapi: any;
    private googleLoaded: boolean = false;
    constructor() {
        window['init'] = (event) => {
            of(gapi).subscribe(
                gapi => {
                    this.gapi = gapi;
                    this.asyncConSubject.next(this.gapi);
                    this.googleLoaded = true;
                },
                error => {
                    console.log("google api error " + error)
                }
            );
        }
    }

    public getGapi() :any {
        return this.gapi;
    }

    public isGoogleLoaded() {
        return this.googleLoaded;
    }

    public createConnection() {
        this.insertGapiLoginMeta();
        this.insertGapiScript();

    }

    public insertGapiLoginMeta() {
        let node = document.createElement('meta');
        node.name = "google-signin-client_id";
        if (environment.production) {
            node.content = prodClientId;
        } else {
            node.content = devClientId;
        }
        document.getElementsByTagName('body')[0].appendChild(node);
    }
    



    public set User(user: any) {
        this.user = user;
    }

    public get User() {
        return this.user;
    }

    public apiAsync(): Observable<any> {
        return this.asyncConStream;
    }

    private insertGapiScript() {
        let node = document.createElement('script');
        node.src = url;
        node.type = 'text/javascript';
        node.async = true;
        node.defer = true;
        document.getElementsByTagName('body')[0].appendChild(node);

    }

    public logoutGoogle() {
        var auth2 = this.gapi.auth2.getAuthInstance();
        auth2.signOut().then(() => {
            this.loginStatus(false);
        });
    }


    public loginStatus(status: boolean) {
        this.googleStatusSubject.next(status);
    }

    public loginStatusStream(): Observable<boolean> {
        return this.googleStatusStream;
    }


}