import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { MaterialModules } from '../../app.module';
import { AuthService } from '../../routing/auth/auth.service';
import { Router } from '@angular/router';
import { AsyncGoogleAPIService } from './google/async-google-api.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[MaterialModules],
      declarations: [ LoginComponent ],
      providers:[
        AuthService,
        AsyncGoogleAPIService,
        { provide: Router, useClass: RouterStub },
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

class RouterStub {
  navigateByUrl(url: string) { return url; }
}
