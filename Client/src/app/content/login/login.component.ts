import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { LoginService } from './login.service';
import { AsyncGoogleAPIService } from './google/async-google-api.service';
import { AuthService } from '../../routing/auth/auth.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
declare const window: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  private gapi:any;
  public auth2: any;
  public googleUser: any;
  constructor(private changeDetectorRef:ChangeDetectorRef,private loginService:LoginService, private asyncGoogleAPIService:AsyncGoogleAPIService, private authService:AuthService, private router:Router) { 
    
  }


  
  public ngOnInit() {
    
    if (!this.asyncGoogleAPIService.isGoogleLoaded()) {
      this.asyncGoogleAPIService.apiAsync().subscribe(
        api => {
          this.gapi = api;
          this.renderButton();
        
          
        }
      )
      this.asyncGoogleAPIService.createConnection();
    } else {
      this.gapi = this.asyncGoogleAPIService.getGapi();
      this.renderButton();
    }
  }



  public signOut() {
    var auth2 = this.gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('signed out of google');
    });
  }

  

  public renderButton() {
    this.gapi.signin2.render('my-signin2', {
      'scope': 'profile email',
      'width': 240,
      'height': 50,
      'longtitle': true,
      'theme': 'dark',
      'onsuccess': this.onSuccess.bind(this),
      'onfailure': this.onFailure.bind(this)
    });

  }

  public onSuccess(user: any): void {
    this.asyncGoogleAPIService.User = user;
    this.loginService.changeLoginState(user.getBasicProfile().getName());
    
  };

  

  public signinChanged(val: any) {
    console.log('Signin state changed to ', val);
  }

  public onFailure(error: any) {
    console.log(error);
  }

  public userChanged(val: any) {
    console.log(val)
  }


}
