import { TestBed, inject } from '@angular/core/testing';

import { LoginService } from './login.service';
import { AuthService } from '../../routing/auth/auth.service';
import { Router } from '@angular/router';

describe('LoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginService,
        AuthService,
        { provide: Router, useClass: RouterStub },]
    });
  });

  it('should be created', inject([LoginService], (service: LoginService) => {
    expect(service).toBeTruthy();
  }));
});


class RouterStub {
  navigate(url: any) { return url; }
}
