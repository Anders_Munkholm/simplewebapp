import { Injectable } from '@angular/core';
import { AuthService } from '../../routing/auth/auth.service';
import { Router } from '@angular/router';

@Injectable()
export class LoginService {

  constructor(private authService:AuthService, private router:Router) { }

  public changeLoginState(username:string):void {
    this.authService.login(username).subscribe(() => {
      if (this.authService.isLoggedIn) {
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
       let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/auth';
 
        // Redirect the user
     
       this.router.navigate([redirect]);
      }
    });
    //notify volatile web storage
    //login into the app, for the user experience
    //the header needs to be notified
    //make sure the user gets a notice that he is indeed logged in, maybe with a material snackbar
    //if a login needs to guard data, do a proper login with a or multiple backends as the guard
  }
}
