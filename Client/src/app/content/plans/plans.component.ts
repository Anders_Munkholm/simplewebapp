import { Component, OnInit } from '@angular/core';
import { PlansService } from './plans.service';
import { Plan } from '../../model/Plan';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.css'],
  providers:[PlansService]
})
export class PlansComponent implements OnInit {

  plans:Array<Plan>;

  constructor(private plansService:PlansService) { }

  ngOnInit() {
    this.initPlans();
  }

  initPlans() {
    this.plans = this.plansService.getPlans();
  }



}
