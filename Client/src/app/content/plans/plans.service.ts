import { Injectable } from '@angular/core';
import { Plan } from '../../model/Plan';
import { Goal } from '../../model/Goal';

@Injectable()
export class PlansService {

  constructor() { }

  getPlans():Plan[] {

    //TODO remove this ugly code and connect to a web api and get the data
    let plans = new Array<Plan>();
    let plan1 = new Plan();
    let plan2 = new Plan();
    let plan3 = new Plan();
    let plan4 = new Plan();

    //plan 1
    plan1.name = "Complete general content";
    plan1.goals = new Array<Goal>();
    let pg11 = new Goal();
    pg11.goal = "Create a plan page";
    pg11.completed = true;
    let pg12 = new Goal();
    pg12.completed = true;
    pg12.goal = "Create a front page";
    let pg13 = new Goal();
    pg13.goal = "Create a cookie page";
    pg13.completed = true;
    let pg14 = new Goal();
    pg14.goal = "Create a GDPR page"
    pg14.completed = true;
    plan1.goals.push(pg11,pg12,pg13,pg14);

     //plan 2
    plan2.name = "Create a connection to a web api";
    plan2.goals = new Array<Goal>();
    let pg21 = new Goal();
    pg21.goal = "Create the web api";
    let pg22 = new Goal();
    pg22.goal = "Connection the web api to a database";
    let pg23 = new Goal();
    pg23.goal = "Create a CI/CD environment for the web api and database";
    plan2.goals.push(pg21,pg22,pg23);

     //plan 3
    plan3.name = "Create the web api login";
    plan3.goals = new Array<Goal>();
    let pg31 = new Goal();
    pg31.goal = "Implement the login";
    let pg32 = new Goal();
    pg32.goal = "Secure the Login in the web api";
    let pg33 = new Goal();
    pg33.goal = "Implement the login in the client";
    plan3.goals.push(pg31,pg32,pg33);


     //plan 4
    plan4.name = "Find something fun to implement";
    plan4.goals = new Array<Goal>();
    let pg41 = new Goal();
    pg41.goal = "Look through old ideas";
    let pg42 = new Goal();
    pg42.goal = "Pick an idea";
    let pg43 = new Goal();
    pg43.goal = "Start planning what to do next";

    plan4.goals.push(pg41,pg42,pg43);

    plans.push(plan1,plan2,plan3,plan4)
    return plans;
  }
}
