import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CookieService {

  constructor() { }

  cookieExist()  {
    return document.cookie.split(';')
    .filter((item) => item.includes('policy_accepted='))
    .length == 1;
  }

  setAcceptedPoliciesCookie() :void {
    document.cookie = "policy_accepted=" + Math.random() + "; expires=Fri, 31 Dec 2018 23:59:59 GMT";
  }
}
