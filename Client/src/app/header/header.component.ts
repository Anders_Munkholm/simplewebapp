import { Component, OnInit ,ChangeDetectorRef} from '@angular/core';

import { AsyncGoogleAPIService } from '../content/login/google/async-google-api.service';
import { HeaderService } from './header.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers:[HeaderService]
})
export class HeaderComponent implements OnInit {

  loggedIn:boolean = false;
  constructor(private router:Router, private asyncGoogleAPIService:AsyncGoogleAPIService, private headerService:HeaderService,private changeDetectorRef:ChangeDetectorRef) { 
    
  }

  ngOnInit() {
    this.listenGoogleLoginStatus();
  }

  public listenGoogleLoginStatus() {
    this.headerService.listenGoogleLoginStatus().subscribe(
      status => {
       this.loggedIn = status; 
       this.changeDetectorRef.detectChanges();// we do this because angular can't update the template automaticly

      }
    )
  }

  goToAuth() {
    this.router.navigate(['/auth'])
  }
  
  goToPlans() {
    this.router.navigate(['/plans'])
  }

  goToContact() {
    this.router.navigate(['/contact']);
  }

  goToLogin() {
      this.router.navigate(['/login']);
  }

  goToHome() {
    this.router.navigate(['/'])
  }

}
