import { TestBed, inject } from '@angular/core/testing';

import { HeaderService } from './header.service';
import { AsyncGoogleAPIService } from '../content/login/google/async-google-api.service';

describe('HeaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeaderService,AsyncGoogleAPIService]
    });
  });

  it('should be created', inject([HeaderService], (service: HeaderService) => {
    expect(service).toBeTruthy();
  }));
});
