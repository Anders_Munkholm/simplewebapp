import { Injectable } from '@angular/core';
import { AsyncGoogleAPIService } from '../content/login/google/async-google-api.service';
import { Observable } from 'rxjs';


@Injectable()
export class HeaderService {

  constructor(private asyncGoogleAPIService:AsyncGoogleAPIService) { }

  public listenGoogleLoginStatus() : Observable<boolean> {
    return this.asyncGoogleAPIService.loginStatusStream();
  }

}
