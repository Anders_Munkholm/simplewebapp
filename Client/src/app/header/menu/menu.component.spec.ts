import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuComponent } from './menu.component';
import { AuthContentService } from '../../content/auth-content/auth-content.service';
import { HeaderService } from '../header.service';
import { Router } from '@angular/router';
import { MaterialModules } from '../../app.module';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[MaterialModules],
      declarations: [ MenuComponent ],
      providers:[
        HeaderService,
        AuthContentService,
        { provide: Router, useClass: RouterStub },]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

class RouterStub {
  navigate(url: any) { return url; }
}
