import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HeaderService } from '../header.service';
import { AsyncGoogleAPIService } from '../../content/login/google/async-google-api.service';
import { Router } from '@angular/router';
import { AuthService } from '../../routing/auth/auth.service';
import { AuthContentService } from '../../content/auth-content/auth-content.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [HeaderService,AuthContentService]
})
export class MenuComponent implements OnInit {

  username: string;
  loggedIn: boolean = false;
  constructor(private authContentService:AuthContentService,private router: Router, private asyncGoogleAPIService: AsyncGoogleAPIService, private headerService: HeaderService, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.listenGoogleLoginStatus();
  }

  public listenGoogleLoginStatus() {
    this.headerService.listenGoogleLoginStatus().subscribe(
      status => {
        if (status) {
          this.username = this.asyncGoogleAPIService.User.getBasicProfile().getName();
        } else {
          this.username = "";
        }
        this.loggedIn = status;
        this.changeDetectorRef.detectChanges();// we do this because angular can't update the template automaticly

      }
    )
  }
  public logout() {
    this.authContentService.logout();
    //reloading the whole site on logout, because google api gets out of angular6's scope on the logout page
    
  }
}
