

export class Cookie {
    name:string;
    description:string;
    origin:string;
    constructor(name:string, description:string,origin:string) {
        this.name = name;
        this.description = description;
        this.origin = origin;
    }
}