import { Size } from "./Size";

export class Area {
    type:string;
    size:Size;
    coordinates:Coordinates;
    
    constructor() {

    }
}