import { Area } from "./Area";

import { Size } from "./Size";
import { StonePattern } from "./StonePattern";

export class Map {
    
    size:Size;
    areas:Area[];
    stonePattern:StonePattern[];
    
    constructor() {
        this.areas = new Array<Area>();
        this.stonePattern = new Array<StonePattern>();
    }
}