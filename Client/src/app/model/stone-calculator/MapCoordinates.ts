

export class MapCoordinates implements Coordinates {
    accuracy: number;
    altitude: number;
    altitudeAccuracy: number;
    heading: number;
    latitude: number;
    longitude: number;
    speed: number;
    type:string;
    
    constructor() {

    }
}