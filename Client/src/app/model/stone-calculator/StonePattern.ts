import { Stone } from "./Stone";
import { Size } from "./Size";

export class StonePattern {
    
    size:Size;
    coordinates:Coordinates;
    stones:Stone[];
    
    constructor() {
        this.stones = Array<Stone>();
    }
}