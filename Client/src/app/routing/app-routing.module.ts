import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import { LoginComponent } from '../content/login/login.component';
import { PageNotFoundComponent } from '../content/page-not-found/page-not-found.component';
import { HomeComponent } from '../content/home/home.component';
import { AuthContentComponent } from '../content/auth-content/auth-content.component';
import { AuthGuard } from './auth/auth-guard.service';
import { ContactComponent } from '../content/contact/contact.component';
import { PlansComponent } from '../content/plans/plans.component';

import { LawPoliciesComponent } from '../content/law-policy/law-policies.component';



const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'plans', component:PlansComponent},
  { path: 'law-policies', component:LawPoliciesComponent},
  { path: 'auth', component:AuthContentComponent, canActivate:[AuthGuard] },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      //{ enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [

  ]
})
export class AppRoutingModule {}