import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthContentComponent } from '../../content/auth-content/auth-content.component';
import { AuthGuard } from './auth-guard.service';




const AuthRoutes: Routes = [


  {
    path: '',
    canActivate: [AuthGuard],
    component: AuthContentComponent,
  }
];



@NgModule({
  imports: [
    RouterModule.forRoot(
      AuthRoutes
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class AuthRoutingModule { }