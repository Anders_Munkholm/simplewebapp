import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { tap, delay } from 'rxjs/operators';




@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isLoggedIn = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  login(name:string): Observable<boolean> {
    //check if the username exist
    if (name != null) {
      return of(true).pipe(
        delay(1000),
        tap(val => this.isLoggedIn = true)
      );
    }
    //do proper checking if you follow a different flow
    return of(false).pipe(
      delay(1000),
      tap(val => this.isLoggedIn = false)
    );
  
  }

  logout(): void {
    this.isLoggedIn = false;
  }
}