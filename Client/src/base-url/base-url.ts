import { environment } from "../environments/environment";

export const base_url = {
    url: environment.production ? 'https://andersmunkholm.com/' : 'http://localhost:4200/'
  };