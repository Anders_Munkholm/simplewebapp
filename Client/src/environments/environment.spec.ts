import { async } from '@angular/core/testing';


import { environment } from './environment';
describe('The Application ', () => {
 
  it('should be deployed in production mode', async(() => {
    expect(environment.production).toBeTruthy();
  }));

});
