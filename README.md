# SimpleWebApp

This is a simple app running @ https://andersmunkholm.com

The app is up and running on a single server. Be gentle! Auto scaling is disabled

# The app contains
1. a simple signin with google
2. some simple gui made with angular-material
3. the header changes once logged in
4. plans and goals


# How is the app tested and deployed?
1. Runs through a CI environment on Visual Studio Team Services
2. Deploys with a CD environment on Visual Studio Team Services to Microsoft Azure 


#Self Note 
use ng test --browsers=Chrome --watch=true for testing
